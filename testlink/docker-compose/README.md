# Docker Compose を利用した環境構築

## 前準備

- Docker インストール
- Docker Compose インストール

## 構成

- サーバ
  - web : Apache 2.x / PHP 7.4, [php:7.4-apache](https://hub.docker.com/_/php)
  - db : MariaDB 10.5, [mariadb:10.5](https://hub.docker.com/_/mariadb)
- ブラウザ
  - Firefox
  - Chrome

## インストール

### web

```
mkdir -p web/app

# install code
git clone -b testlink_1_9 \
  --depth 1 \
  -- https://github.com/TestLinkOpenSourceTRMS/testlink-code.git \
  web/app

# settings
chmod -R a+w web/app

mkdir -p web/data/{logs,upload_area}

chmod -R a+w web/data
```

```
mkdir -p web/conf

docker run \
  -i \
  --rm \
  -v $PWD/web/conf:/sample \
  php:7.4-apache \
    bash -c 'cp $PHP_INI_DIR/php.ini-production /sample/php.ini'

sudo chown -R $(id -u):$(id -g) web/conf

sed -i -e "s/;extension=mysqli/extension=mysqli/g" web/conf/php.ini
```

## 操作

```
# start
docker-compose -p testlink up -d

# setup
## web
docker-compose -p testlink exec \
  web \
    bash -c "docker-php-ext-install mysqli"

docker-compose -p testlink restart web

## db
docker-compose -p testlink exec db mysql -hdb -uroot -proot -e "CREATE DATABASE IF NOT EXISTS testlink"
docker-compose -p testlink exec db mysql -hdb -uroot -proot -e "SHOW DATABASES"

docker-compose -p testlink exec db mysql -hdb -uroot -proot -e "CREATE USER IF NOT EXISTS testlink IDENTIFIED BY 'testlink'" testlink
docker-compose -p testlink exec db mysql -hdb -uroot -proot -e "CREATE USER IF NOT EXISTS testlink@localhost IDENTIFIED BY 'testlink'" testlink
docker-compose -p testlink exec db mysql -hdb -uroot -proot -e "SELECT host,user FROM mysql.user" testlink

docker-compose -p testlink exec db mysql -hdb -uroot -proot testlink
GRANT SELECT, UPDATE, DELETE, INSERT ON `testlink`.* TO testlink WITH GRANT OPTION;
GRANT SELECT, UPDATE, DELETE, INSERT ON `testlink`.* TO testlink@localhost WITH GRANT OPTION;
SHOW GRANTS FOR testlink;
SHOW GRANTS FOR testlink@localhost;
\q
```

http://localhost:18080/testlink


- `db:3306`
- root / root
- testlink / testlink

login name: admin
password : admin 

```
IMPORTANT NOTICE - IMPORTANT NOTICE - IMPORTANT NOTICE - IMPORTANT NOTICE

YOU NEED TO RUN MANUALLY Following Script on your DB CLIENT Application

/var/www/html/testlink/install/sql/mysql/testlink_create_udf0.sql

THANKS A LOT


YOUR ATTENTION PLEASE:
To have a fully functional installation You need to configure mail server settings, following this steps

    copy from config.inc.php, [SMTP] Section into custom_config.inc.php.
    complete correct data regarding email addresses and mail server.


Installation was successful!
You can now log in to Testlink (using login name:admin / password:admin - Please Click Me!).
```

初期セットアップ後に再セットアップする場合は DB と `config_db.inc.php` を削除

```
docker-compose -p testlink up -d

docker-compose -p testlink ps

docker-compose -p testlink logs

docker-compose -p testlink stop
docker-compose -p testlink start

docker-compose -p testlink restart

docker-compose -p testlink down --volumes
```

