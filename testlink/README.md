# TestLink

https://github.com/TestLinkOpenSourceTRMS/testlink-code

## System Requirements - server

Server environment should consist of:

- web-server: Apache 2.x
- PHP > 5.5 It will be better if you use PHP 7.2.x
- PHP IMPORTANTE NOTICE: next TestLink Version will require minimum PHP 7.3.x
- DBMS
  - MySQL 5.7.x
    - The `log_bin_trust_function_creators` option must be enabled.
  - MariaDB 10.1.x
    - The `log_bin_trust_function_creators` option must be enabled.
  - Postgres 9.x
  - MS-SQL 201x -> SUPPORT IS INCOMPLETE

Tested on web-browsers:

- Firefox
- Chrome

ATTENTION: we have not enough resources to test on all kind of browsers. Right now development is done using Chrome & Firefox.
